#!/bin/bash

# shell to use
#$ -S /bin/bash

#$ -M thomas.chaussepied@wanadoo.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea
#$ -N Concatenate_Brapa_Scaffold

# if the script should be run in the current directory
#$ -cwd



## AIMS    : to concatenate gff3 files after REPET
## USAGE   : ./Concatenate.sh path/to/gff3/file/
## NOTE    : 
## AUTHORS : thomas.chaussepied@wanadoo.fr


# init env X

###############################################################################

echo "##gff-version 3" >> tmp
grep -h "##s" $1/*.gff3 >> tmp
for file in `ls $1/ | grep "gff3"`
do
	grep -vh "##" $1/$file  >> tmp
	echo "###" >> tmp	
done
sed '$d' tmp > final.gff3
rm tmp

###############################################################################s
