#! /bin/bash
#shell to use
#$ -S /bin/bash

#$ -M thomas.chaussepied@wanadoo.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea
#$ -j y 

# if the script should be run in the current directory
#$ -cwd



## AIMS    : BLAST and output in bed format
## USAGE   : ./blast_to_bed.sh reference.fa query.fa
## NOTE    :
## AUTHORS : thomas.chaussepied@wanadoo.fr

. /local/env/envblast+.sh  
###############################################################################


set -o nounset # exit on unset variable.
set -o errexit # exit on unexpected error.

declare -r INFILE_ref="${1}"
declare -r INFILE_query="${2}"
name_ref=${INFILE_ref##*/}
OUTFILE=`echo "${INFILE_query%%.*}"_"${name_ref%%.*}"`"_blastx.bed"

function printVars ()
{
        printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
        printf "╟ - SCRIPT                      = $0\n"
        printf "╟ - INFILE : query		= $INFILE_query\n"
        printf "╟ - INFILE : ref		= $INFILE_ref\n"
        printf "╟ - OUTFILE                     = $OUTFILE\n"
        printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

printVars >&2

###############################################################################
#makeblastdb -in $INFILE_ref -dbtype prot -title db_ref -out db_file
blastx -query $INFILE_query -subject $INFILE_ref -evalue 10e-20 -outfmt "6 qseqid sseqid qstart qend" -out $OUTFILE

#qblastn -query $INFILE_query -subject $INFILE_ref -evalue 10e-20 -outfmt "6 qseqid" -out $OUTFILE
###############################################################################
printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0

